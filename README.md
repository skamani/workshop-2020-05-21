# Workshop 2020-05-21

This Project is created in support of the Workshop presented on May 21, 2020.  There are resources here to use during the workshop if necessary.

## About GitLab
GitLab is a complete DevOps platform, delivered as a single application. This makes GitLab unique and makes Concurrent DevOps possible, unlocking your organization from the constraints of a pieced together toolchain. Join us for a live Q&A to learn how GitLab can give you unmatched visibility and higher levels of efficiency in a single application across the DevOps lifecycle.

Read the [Data Science with GitLab](https://about.gitlab.com/solutions/data-science/) page to see how GitLab can help.


## Reference Projects

There are many options for Data Science projects.  The [Cookie Cutter Data Science project](https://drivendata.github.io/cookiecutter-data-science/) is very helpful in 
preparing a standardized project structure.  This increases its ability to be shared and reused.

There are examples in this repository of code and references for various languages

## SAS
https://ms.mcmaster.ca/peter/s4p03/s4p03_0304/sasnotes.htm

## R
- [R Statistics Cookbook](https://github.com/PacktPublishing/R-Statistics-Cookbook)
- A very useful blog post that guides the users to getting lots of value out of R by automating the project in GitLab.
  - https://jozef.io/r106-r-package-gitlab-ci/

## Python
- GitLab [Data team](https://about.gitlab.com/handbook/business-ops/data-team/) uses various Python constructs to create corporate dashboards
- [Climate Statistics project](https://gitlab.com/willyhagi/climate-statistics/-/tree/master)

## Gitlab for Data Science
- [Jupyter Notebook files](https://docs.gitlab.com/ee/user/project/repository/jupyter_notebooks/) 
- [Using GitLab CI/CD to run data-science projects on AWS Fargate](https://medium.com/@stijnvanorbeek/using-gitlab-ci-cd-to-run-data-science-projects-on-aws-a908522f5b8b)




